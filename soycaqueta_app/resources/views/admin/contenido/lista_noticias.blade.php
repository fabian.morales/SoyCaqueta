@extends('admin')

@section('content')
<div class="row titulo lista">
    <div class="small-12 columns">Editar noticias</div>
</div>
<div class="row">
    <div class="small-12 columns">
        <a href="{{ route('admin::contenido::noticia::crear') }}" class="button alert">Noticia nueva <i class="fi-plus"></i></a>
    </div>
</div>
<br />
<div class="row item lista head">
    <div class="small-5 columns">Nombre</div>
    <div class="small-3 columns">Categoría</div>
    <div class="small-2 columns">Estado</div>
    <div class="small-2 columns">Acctiones</div>
</div>
@foreach($noticias as $n)
<div class="row item lista" @if($n->activo == "N")style="color: #f00"@endif>
    <div class="small-5 columns">{{ $n->titulo }}</div>
    <div class="small-3 columns">@if(sizeof($n->categoria)){{ $n->categoria->nombre }}@else Sin Categoría @endif</div>
    <div class="small-2 columns">@if($n->activo == "S") Publicado @else Despublicado @endif</div>
    <div class="small-2 columns">
        <a class="tooltip-x" title='Editar contenido' href="{{ route('admin::contenido::noticia::editar', ['id' => $n->id]) }}"><i class="fi-pencil"></i></a>
        @if($n->activo == 'S')
        <a class="tooltip-x" title='Despublicar' href="{{ route('admin::contenido::estado', ['id' => $n->id, 'activo' => 'N']) }}"><i class="fi-x-circle"></i></a>
        @else
        <a class="tooltip-x" title='Publicar' href="{{ route('admin::contenido::estado', ['id' => $n->id, 'activo' => 'S']) }}"><i class="fi-check"></i></a>
        @endif
    </div>
</div>
@endforeach

@stop