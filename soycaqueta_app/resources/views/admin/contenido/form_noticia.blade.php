@extends('admin')

@section('js_header')
<script src="{{ asset('/js/tinymce/tinymce.min.js') }}"></script>
<script>
    (function(window, $){
        $(document).ready(function() {
            tinymce.init({
                selector: "textarea#cuerpo",
                height: 300,
                theme: 'modern',
                language: 'es',
                relative_urls: false,
                document_base_url: 'http://soycaqueta.frontiersoft.info/',
                remove_script_host: false,
                extended_valid_elements: 'span,iframe[src|frameborder|style|scrolling|class|width|height|name|align]',
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor varsecciones",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste filemanager textcolor"
                ],
                toolbar: "insertfile undo redo | styleselect | forecolor backcolor | bold italic fontsizeselect | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | visualblocks",
                image_advtab: true
            });
            
            $("#btnAgregarVar").click(function(e) {
                e.preventDefault();
                tinymce.activeEditor.execCommand('mceInsertContent', false, $("#variable").val());
            });
        });
    })(window, jQuery);

</script>
@stop

@section('content')
<div class="row">
    <div class="small-12 columns">
        <h3 class="titulo seccion"><span>Editar noticia</span></h3>
    </div>
</div>
<form id="form_email" name="form_email" action="{{ route('admin::contenido::guardar') }}" method="post" enctype="multipart/form-data">
    <input type="hidden" id="id" name="id" value="{{ $noticia->id }}" />
    <input type="hidden" id="tipo" name="tipo" value="N" />
    <input type="hidden" id="llave" name="llave" value="{{ $noticia->id }}" />
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <input type='hidden' name='peso' id='peso' value='0' />    
    <div class="row">
        <div class="small-12 columns">
            <label for="titulo">T&iacute;tulo</label>
            <input type='text' name='titulo' id='titulo' value='{{ $noticia->titulo }}' />
        </div>
        <div class="small-12 columns">
            <label for="id_categoria">Categoría</label>
            <select id="id_categoria" name="id_categoria">
                <option value="">Sin categoría</option>
                @foreach($categorias as $c)
                <option value="{{ $c->id }}" @if($c->id == $noticia->id_categoria) selected @endif>{{ $c->nombre }}</option>
                @endforeach
            </select>
        </div>
        <div class="small-12 columns">
            <label for="destacado">
                Destacar
                <input type='hidden' name='inicio' id='inicio' value='N' />
                <input type='checkbox' name='destacado' id='destacado' value='S' @if($noticia->destacado == 'S') checked="checked" @endif />
            </label>
        </div>
        <div class="small-12 columns">
            <label for="activo">
                Publicado
                <input type='checkbox' name='activo' id='activo' value='S' @if($noticia->activo == 'S') checked="checked" @endif />
            </label>
        </div>
        <div class="small-12 columns">
            <label for="imagen">Seleccione una imagen para la noticia <input type="file" name="imagen" id="imagen" /></label>
        </div>
        <div class="small-12 columns">
            <label for="nombre">Contenido de la secci&oacute;n</label>
        </div>
        <div class="small-12 columns">
            <textarea id='cuerpo' name='cuerpo'>{{ $noticia->cuerpo }}</textarea>
        </div>
    </div>
    
    <div class="row">
        <div class="small-12 columns">
            <a class="button alert" href="{{ route('admin::contenido::noticia::lista') }}" />Cancelar</a>
            <input type="submit" value="Guardar" class="button default" />
        </div>
    </div>
</form>
@stop