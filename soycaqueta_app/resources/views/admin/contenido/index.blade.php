@extends('admin')

@section('content')
<div class="row titulo lista">
    <div class="small-12 columns">Editar Emails</div>
</div>
<div class="row item lista head">
    <div class="small-10 columns">Nombre</div>
    <div class="small-2 columns">Editar</div>
</div>
@foreach($emails as $i=>$e)
<div class="row item lista">
    <div class="small-10 columns">{{ $e }}</div>
    <div class="small-2 columns"><a class="tooltip-x" title='Editar email' href="{{ url('administrador/contenido/editar/'.$i) }}"><i class="fi-pencil"></i></a></div>
</div>
@endforeach

@stop