@extends('admin')

@section('content')
<div class="row">
    <div class="small-12 columns">
        <h3 class="titulo seccion"><span>Datos del item de menu</span></h3>
    </div>
</div>
<form id="form_menu" name="form_menu" action="{{ url('administrador/menus/guardar') }}" method="post" enctype="multipart/form-data">
    <input type="hidden" id="id" name="id" value="{{ $menu->id }}" />
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="titulo">Nombre</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="titulo" id="titulo" value="{{ $menu->titulo }}" />
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="llave">Llave</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="llave" id="llave" value="{{ $menu->llave }}" />
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="peso">Peso (orden)</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="peso" id="peso" value="{{ $menu->peso }}" />
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="url">Url</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="url" id="url" value="{{ $menu->url }}" />
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="id_seccion">Secci&oacute;n</label>
        </div>
        <div class="medium-8 small-12 columns">
            <select id="id_seccion" name="id_seccion">
            @foreach($secciones as $s)
                <option value="{{ $s->id }}" @if($s->id == $menu->id_seccion) selected="selected" @endif>{{ $s->titulo }}</option>
            @endforeach
            </select>
        </div>
    </div>
    
    <div class="row">
        <div class="small-12 columns">
            <a class="button alert" href="{{ url('/administrador/menus/') }}" />Cancelar</a>
            <input type="submit" value="Guardar" class="button default" />
        </div>
    </div>
</form>
@stop