<div class="row">
    <div class="small-12 columns">
        <a href="{{ route('admin::categoria::crear') }}" class="button alert">Categor&iacute;a nueva <i class="fi-plus"></i></a>
    </div>
</div>
<div class="row titulo lista">
    <div class="small-12 columns">Lista de categor&iacute;as</div>
</div>
<div class="row item lista head">
    <div class="small-2 columns">N&uacute;m</div>
    <div class="small-4 columns">Nombre</div>
    <div class="small-3 columns">Tipo</div>
    <div class="small-3 columns">Editar</div>
</div>
@foreach($categorias as $c)
<div class="row item lista">
    <div class="small-2 columns">{{ $c->id }}</div>
    <div class="small-4 columns">{{ $c->nombre }}</div>
    <div class="small-3 columns">{{ $tipos[$c->layout] }}</div>
    <div class="small-3 columns"><a class="tooltip-x" title='Editar categoría' href="{{ route('admin::categoria::editar', ['id' => $c->id]) }}"><i class="fi-pencil"></i></a></div>
</div>
@endforeach
<div class="row">
    <div class="small-12 columns text-center">
        {!! $categorias->render() !!}
    </div>
</div>