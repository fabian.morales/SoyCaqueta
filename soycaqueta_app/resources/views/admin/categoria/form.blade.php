@extends('admin')

@section('js_header')

@stop

@section('content')
<div class="row">
    <div class="small-12 columns">
        <h3 class="titulo seccion"><span>Editar categoría</span></h3>
    </div>
</div>
<form id="form_categoria" name="form_categoria" action="{{ route('admin::categoria::guardar') }}" method="post" enctype="multipart/form-data">
    <input type="hidden" id="id" name="id" value="{{ $categoria->id }}" />
    <input type="hidden" id="llave" name="llave" value="{{ $categoria->id }}" />
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="small-12 columns">
            <label for="titulo">T&iacute;tulo</label>
            <input type='text' name='nombre' id='titulo' value='{{ $categoria->nombre }}' />
        </div>
        <div class="small-12 columns">
            <label for="layout">Tipo</label>
            <select id="layout" name="layout">
                @foreach($tipos as $key => $t)
                <option value="{{ $key }}" @if($categoria->layout == $key) selected @endif>{{ $t }}</option>
                @endforeach
            </select>
        </div>
    </div>
    
    <div class="row">
        <div class="small-12 columns">
            <a class="button alert" href="{{ route('admin::categoria::lista') }}" />Cancelar</a>
            <input type="submit" value="Guardar" class="button default" />
        </div>
    </div>
</form>
@stop