@extends('master')

@section('js_header')

@stop

@section('content')
<section class="noticia cat">
    <div class='row'>
        <div class='small-12 columns'>
            <h1 class="titulo noticia">Blog</h1>
        </div>
    </div>
    
    <div class="row">
        <div class="medium-9 columns">
            <div class="row">
                @foreach($noticias as $n)
                <div class="medium-3 columns">
                    <img src="{{ asset('imagenes/contenido/'.$n->id.'.jpg') }}" class="float-left" />
                </div>
                <div class="medium-9 columns end">
                    <div class="bloque noticias">
                        <div class="noticia clearfix">
                            @if(sizeof($n->categoria))
                            <a href="{{ url('/noticias/'.$n->categoria->id.'-'.$n->categoria->llave.'/'.$n->id.'-'.$n->llave) }}">
                            @else
                            <a href="{{ url('/noticias/general/'.$n->id.'-'.$n->llave) }}">
                            @endif

                            <div class="fecha">{{ $n->created_at->format('d M Y') }}</div>
                            <h3>{{ $n->titulo }}</h3>
                            </a>
                            <div>
                                @for($i=1;$i<=5;$i++)
                                <i class="fi-star @if($n->promedio_puntaje >= $i) amarillo @else gris @endif"></i>
                                @endfor
                            </div>
                            <p>
                                {{ strtok(wordwrap(strip_tags($n->cuerpo), 200, "\n"), "\n") }}
                            </p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="medium-3 columns">
            <h2 class="titulo mas-leidas"><span>Categor&iacute;as</span></h2>
            <ul class="menu-blog">
                @foreach($categorias as $c)
                <li><a href="{{ url('/blog/'.$c->id.'-'.$c->llave) }}">{{ $c->nombre }}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
    
    <div class='row'>
        <div class='small-12 columns'>
            {!! $noticias->render() !!}
        </div>
    </div>

</section>
@stop