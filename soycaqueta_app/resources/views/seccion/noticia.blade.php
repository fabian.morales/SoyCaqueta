@extends('master')

@section('js_header')

@stop

@section('content')
<section class="noticia">
    <div class='row'>
        <div class='small-12 columns'>
            <h1 class="titulo noticia">Noticias</h1>
        </div>
    </div>

    <div class="row">
        <div class="medium-8 columns">
            @if(sizeof($noticia))
            <div class='row collapse'>
                <div class='small-12 columns'>
                    <div class="contenido">
                        <h2 class="titulo noticia">{{ $noticia->titulo }}</h2>
                    </div>
                    <br />
                    <div class="imagen-noticia">
                        <img src="{{ asset('imagenes/contenido/'.$noticia->id.'.jpg') }}" class="img-fullwidth" />
                    </div>
                </div>
                @if(sizeof($noticia->categoria))
                <div class="medium-4 columns">
                    <div class="info"><strong>Categoria</strong><br /><small>{{ $noticia->categoria->nombre }}</small></div>
                </div>
                @endif
                <div class="medium-4 columns">
                    <div class="info"><strong>Autor</strong><br /><small>{{ $noticia->autor->nombre }}</small></div>
                </div>
                <div class="medium-4 columns end">
                    <div class="info"><strong>{{ $noticia->num_comentarios }}</strong><br /><small>@if($noticia->num_comentarios == 1) comentario @else comentarios @endif</small></div>
                </div>
            </div>

            <div class='row'>
                <div class='small-12 columns'>
                    {!! $noticia->cuerpo !!}
                </div>
                <div class='small-12 columns'>
                    <hr />
                    @if(sizeof($noticia->valoraciones))
                    Tu valoraci&oacute;n: 
                    
                    @for($i=1;$i<=5;$i++)
                    <i class="fi-star @if($noticia->valoraciones[0]->valor >= $i) amarillo @else gris @endif"></i>
                    @endfor
                    
                    @elseif(Auth::check())
                    
                    Valora esta noticia:
                    
                    @foreach($noticia->generarTokensVal() as $i => $t)
                    <a href="{{ url('/valorar/'.$t) }}" class="gris valoracion"><i class="fi-star"></i></a>
                    @endforeach
                    
                    @else
                    <p><strong>Debes iniciar sesi&oacute;n para valorar esta noticia</strong></p>
                    @endif
                </div>
            </div>
            
            @endif
        </div>
            <div class="medium-4 columns">
            <h2 class="titulo noticias"><span>Noticias</span></h2>
            <div class="bloque noticias">
                @foreach($recientes as $r)
                <div class="noticia">
                    @if(sizeof($r->categoria))
                    <a href="{{ url('/noticias/'.$r->categoria->id.'-'.$r->categoria->llave.'/'.$r->id.'-'.$r->llave) }}">
                    @else
                    <a href="{{ url('/noticias/general/'.$r->id.'-'.$r->llave) }}">
                    @endif
                    <h3>{{ $r->titulo }}</h3>
                    </a>
                    <div class="azul small">
                        {{ $r->created_at->format('d M Y') }} //
                        @if(sizeof($r->categoria))
                        {{ $r->categoria->nombre }} //
                        @endif
                        {{ $noticia->num_comentarios }} @if($noticia->num_comentarios == 1) comentario @else comentarios @endif
                    </div>
                    <div>
                        @for($i=1;$i<=5;$i++)
                        <i class="fi-star @if($r->promedio_puntaje >= $i) azul @else gris @endif"></i>
                        @endfor
                    </div>
                </div>
                @endforeach
            </div>
            
            <h2 class="titulo mas-leidas"><span>M&aacute;s le&iacute;das</span></h2>
            <div class="bloque noticias">
                @foreach($visitadas as $v)
                <div class="noticia clearfix">
                    @if(sizeof($v->categoria))
                    <a href="{{ url('/noticias/'.$v->categoria->id.'-'.$v->categoria->llave.'/'.$v->id.'-'.$v->llave) }}">
                    @else
                    <a href="{{ url('/noticias/general/'.$v->id.'-'.$v->llave) }}">
                    @endif
                    <img src="{{ asset('imagenes/contenido/'.$v->id.'.jpg') }}" class="float-left" />
                    <div class="fecha">{{ $v->created_at->format('d M Y') }}</div>
                    <h3>{{ $v->titulo }}</h3>
                    </a>
                    <div>
                        @for($i=1;$i<=5;$i++)
                        <i class="fi-star @if($v->promedio_puntaje >= $i) amarillo @else gris @endif"></i>
                        @endfor
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <h2 class="titulo mas-leidas">
                <span>
                    {{ $noticia->num_comentarios }} @if($noticia->num_comentarios == 1) comentario @else comentarios @endif
                </span>
            </h2>
            @if (Auth::check())
            <a href="{{ url('/comentar/'.$noticia->generarToken()) }}" class="button" data-featherlight>Comentar</a>
            @else
            <h3 class="text-center">Para comentar, debes iniciar sesi&oacute;n</h3>
            @endif
            <br />
            @include('seccion.comentarios', array("comentarios" => $noticia->comentarios, "noticia" => $noticia))
        </div>
    </div>
</section>
@stop