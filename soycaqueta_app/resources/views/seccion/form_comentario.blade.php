<form name="form_comentar" id="form_comentar" method="post" action="{{ url("/comentar") }}">
    <input type="hidden" id="token" name="token" value="{{ $token }}" />
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="small-12 columns">
            <p>Escribe tu comentario</p>
            <textarea id="comentario" rows="4" cols="50" name="comentario"></textarea>
            <input type="submit" value="Comentar" class="button" />
        </div>
    </div>
</form>