@foreach($comentarios as $c)
<div class="comentario">
    <div class="contenido-com">
        @if (Auth::check())        
        @endif
        <div class="fecha-com text-center">
            <i class="fi-comment-quotes"></i><br />
            {{ $c->created_at->format('d M Y') }}
        </div>
        <strong><i>{{ $c->usuario->nombre }}</i></strong> dijo:
        <p>{{ $c->contenido }}</p>
        <br />
        <a href="{{ url('/comentar/'.$noticia->generarToken($c->id)) }}" class="boton-responder" data-featherlight>Responder <i class="fi-loop"></i></a>
    </div>
    @include('seccion.comentarios', array("comentarios" => $c->hijos()->get(), "noticia" => $noticia))
</div>
@endforeach