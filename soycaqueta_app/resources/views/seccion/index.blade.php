@extends('master')

@section('js_header')
@stop

@section('content')
    <section id="banner_home">
        <div class="row">
            <div class="small-12 medium-8 columns">
                <ul id="slider_home">
                    @foreach($destacadas as $d)
                    <li>
                        @if(sizeof($d->categoria))
                        <a href="{{ url('/noticias/'.$d->categoria->id.'-'.$d->categoria->llave.'/'.$d->id.'-'.$d->llave) }}">
                        @else
                        <a href="{{ url('/noticias/general/'.$d->id.'-'.$d->llave) }}">
                        @endif
                            <img src="{{ asset('imagenes/contenido/'.$d->id.'.jpg') }}" />
                            <span><strong><big>{{ $d->titulo }}</big></strong><br />
                            {{ strtok(wordwrap(strip_tags($d->cuerpo), 60, "\n"), "\n") }}</span>
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="small-12 medium-4 columns">
                <ul class="carrusel_home">
                    @foreach($destacadas as $d)
                    <li>
                        <div class="row">
                            <div class="small-3 columns">
                                <img src="{{ asset('imagenes/contenido/'.$d->id.'.jpg') }}" />
                            </div>
                            <div class="small-9 columns">
                                <span>
                                    @if(sizeof($d->categoria))
                                    <a href="{{ url('/noticias/'.$d->categoria->id.'-'.$d->categoria->llave.'/'.$d->id.'-'.$d->llave) }}">
                                    @else
                                    <a href="{{ url('/noticias/general/'.$d->id.'-'.$d->llave) }}">
                                    @endif
                                        {{ $d->titulo }}
                                    </a>
                                </span>
                                <br />
                                <span class="small gris">
                                    @if($d->num_comentarios == 1)
                                    1 comentario
                                    @else
                                    {{ $d->num_comentarios }} comentarios
                                    @endif
                                    // {{ $d->created_at->format('d M') }}
                                </span>
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </section>
    
    <section id="noticias_home">
        <div class="row">
            <div class="small-12 medium-4 columns">
                <h2 class="titulo noticias"><span>Noticias</span></h2>
                <div class="bloque noticias destacado">
                    @foreach($recientes as $i => $r)
                    <div class="noticia clearfix">
                        @if(sizeof($r->categoria))
                        <a href="{{ url('/noticias/'.$r->categoria->id.'-'.$r->categoria->llave.'/'.$r->id.'-'.$r->llave) }}">
                        @else
                        <a href="{{ url('/noticias/general/'.$r->id.'-'.$r->llave) }}">
                        @endif
                        <img src="{{ asset('imagenes/contenido/'.$r->id.'.jpg') }}" class="float-left" />
                        <h3>{{ $r->titulo }}</h3>
                        <div class="fecha">{{ $r->created_at->format('d M Y') }} @if($i == 0) // {{ $r->num_comentarios }} comentario(s) @endif</div>
                        </a>
                        <div>
                            @for($i=1;$i<=5;$i++)
                            <i class="fi-star @if($r->promedio_puntaje >= $i) amarillo @else gris @endif"></i>
                            @endfor
                        </div>
                        <span>{{ strtok(wordwrap(strip_tags($r->cuerpo), 80, "\n"), "\n") }}</span>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="small-12 medium-4 columns">
                <h2 class="titulo populares"><span>M&aacute;s populares</span></h2>
                <div class="bloque noticias destacado">
                    @foreach($populares as $i => $p)
                    <div class="noticia clearfix">
                        @if(sizeof($p->categoria))
                        <a href="{{ url('/noticias/'.$p->categoria->id.'-'.$p->categoria->llave.'/'.$p->id.'-'.$p->llave) }}">
                        @else
                        <a href="{{ url('/noticias/general/'.$p->id.'-'.$p->llave) }}">
                        @endif
                        <img src="{{ asset('imagenes/contenido/'.$p->id.'.jpg') }}" class="float-left" />
                        <h3>{{ $p->titulo }}</h3>
                        <div class="fecha">{{ $p->created_at->format('d M Y') }} @if($i == 0) // {{ $r->num_comentarios }} comentario(s) @endif</div>
                        </a>
                        <div>
                            @for($i=1;$i<=5;$i++)
                            <i class="fi-star @if($p->promedio_puntaje >= $i) amarillo @else gris @endif"></i>
                            @endfor
                        </div>
                        <span>{{ strtok(wordwrap(strip_tags($p->cuerpo), 80, "\n"), "\n") }}</span>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="small-12 medium-4 columns">
                <h2 class="titulo mas-leidas"><span>M&aacute;s le&iacute;das</span></h2>
                <div class="bloque noticias">
                    @foreach($visitadas as $v)
                    <div class="noticia clearfix">
                        @if(sizeof($v->categoria))
                        <a href="{{ url('/noticias/'.$v->categoria->id.'-'.$v->categoria->llave.'/'.$v->id.'-'.$v->llave) }}">
                        @else
                        <a href="{{ url('/noticias/general/'.$v->id.'-'.$v->llave) }}">
                        @endif
                        <img src="{{ asset('imagenes/contenido/'.$v->id.'.jpg') }}" class="float-left" />
                        <div class="fecha">{{ $v->created_at->format('d M Y') }}</div>
                        <h3>{{ $v->titulo }}</h3>
                        </a>
                        <div>
                            @for($i=1;$i<=5;$i++)
                            <i class="fi-star @if($v->promedio_puntaje >= $i) amarillo @else gris @endif"></i>
                            @endfor
                        </div>
                    </div>
                    @endforeach
                </div>
                <!--br />
                <h2 class="titulo tag"><span>Tags</span></h2>
                <div class="tags">
                    <span>tag uno</span>
                    <span>tag dos</span>
                    <span>tag tres</span>
                    <span>tag cuatro</span>
                    <span>tag cinco</span>
                    <span>tag seis</span>
                    <span>tag siete</span>
                    <span>tag ocho</span>
                    <span>tag nueve</span>
                    <span>tag diez</span>
                </div-->
            </div>
        </div>
    </section>
@stop