<h3>Detalles</h3>
<hr />
<table style="border-top: 1px solid #333; border-right: 1px solid #333; width: 100%;" cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px; font-weight: bold; background-color: #ddd;">Producto</th>
            <th style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px; font-weight: bold; background-color: #ddd;">Enlace (URL)</th>
            <th style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px; font-weight: bold; background-color: #ddd;">Cantidad</th>
            @if ($pedido->estado != "N")
            <th style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px; font-weight: bold; background-color: #ddd;">Valor</th>
            @endif
        </tr>
    </thead>
    <tbody>
        @foreach($pedido->detalles as $d)
        <tr>
            <td style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px;">{{ $d->nombre }}</td>
            <td style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px;">
                @if(!empty($d->link))<a href="{{ \App\Detalle::addhttp($d->link) }}" target="_blank">Ver</a>@else &nbsp; @endif
            </td>
            <td style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px;">{{ $d->cantidad }}</td>
            @if ($pedido->estado != "N")            
            <td style="border-bottom: 1px solid #333; border-left: 1px solid #333; padding: 5px;">@if($d->valor > 0)${{ Helper::number_format($d->valor) }}@else - @endif</td>
            @endif
        </tr>
        @endforeach
    </tbody>
</table>