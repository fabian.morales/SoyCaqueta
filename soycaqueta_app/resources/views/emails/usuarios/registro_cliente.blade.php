@extends('emails')

@section('content')
    <img src="{{ asset('imagenes/logo.png') }}" />
    <p>       
        Apreciado {{ $cliente->nombre }}, <br />

        Su registro ha sido exitoso
    </p>

    <p>Gracias por escoger <a href="http://encarguelo.com" target="_blank">Encarguelo.com</a></p>
@stop
