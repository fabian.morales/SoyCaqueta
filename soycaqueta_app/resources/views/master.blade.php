
<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]-->

<head>
    <title>Soy Caquet&aacute;</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <meta name="keywords" content="" />
    <meta name="description" content="" />

    <!-- Favicon --> 
    <link rel="shortcut icon" href="{{ asset('template/images/favicon.ico') }}">

    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- ######### CSS STYLES ######### -->

    <link rel="stylesheet" href="{{ asset('foundation/css/foundation.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('foundation/fonts/foundation-icons.css') }}" />
    <link rel="stylesheet" href="{{ asset('js/lightslider/css/lightslider.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('js/featherlight/featherlight.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    @section ('css_header')
    @show

    @section ('js_header')
    @show
</head>

<body>
    <div id="loader"><div></div></div>

    <header>
        <div class="row">
            <div class="small-12 medium-3 columns">
                <img src="{{ asset('imagenes/logo.png') }}" />
            </div>
            <div class="small-12 medium-7 columns relativo">               
                <a href="#" class="boton-menu"></a>
                <nav>
                    <ul>
                        <li><a href='{{ url('/') }}'>Inicio</a></li>
                        @foreach($main_menu as $m)

                        @if(!empty($m->url))
                        <li><a href='{{ url($m->url) }}'>{{ $m->titulo }}</a></li>
                        @else
                        <li><a href='{{ url('/'.$m->llave) }}'>{{ $m->titulo }}</a></li>
                        @endif

                        @endforeach
                    </ul>
                </nav>
            </div>
            
            <div class="small-12 medium-2 columns text-right">
                <ul class="redes sociales">
                    <li><a href="#" class="facebook"></a></li>
                    <li><a href="#" class="twitter"></a></li>
                </ul>

                @if(Auth::check())
                    <a class="item perfil">Est&aacute;s logueado como {{ Auth::user()->email }}&nbsp;</a>
                    <a class='item perfil' href='{{ url('/usuario/perfil') }}'>Mi cuenta</a>
                    <a class='item logout' href='{{ url('/logout') }}'>Cerrar sesi&oacute;n</a>
                    @if(Auth::user()->admin == 'Y')
                    &nbsp;-&nbsp;<a class='item' href='{{ url('/administrador') }}'>Administrador</a>
                    @endif
                @else
                    <a class='item perfil' href='{{ url('/login') }}'>Iniciar sesi&oacute;n</a>
                @endif
            </div>
        </div>
    </header>

    <div class="row">
        <div class="small-12 columns">
            <div class="mensajes">
                @if (Session::has('mensajeError'))
                <div class="alert callout" data-closable>
                    {{ Session::get('mensajeError') }}
                    <button class="close-button" aria-label="Cerrar" type="button" data-close>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                @if (Session::has('mensaje'))
                <div class="success callout" data-closable>
                    {{ Session::get('mensaje') }}
                    <button class="close-button" aria-label="Cerrar" type="button" data-close>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                @if (Session::has('mensajeAviso'))
                <div class="warning callout" data-closable>
                    {{ Session::get('mensajeAviso') }}
                    <button class="close-button" aria-label="Cerrar" type="button" data-close>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                @if (Session::has('mensajeExt'))
                {{ Session::get('mensajeExt') }}
                @endif
                @if (Session::has('status'))
                <div class="success callout" data-closable>
                    {{ Session::get('status') }}
                    <button class="close-button" aria-label="Cerrar" type="button" data-close>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
            </div>
        </div>
    </div>

    @yield('content')


    <footer>
        <div class="row">
            <div class="small-12 columns">
                2016. Todos los derechos reservados.
            </div>
        </div>
    </footer>


    <!-- ######### JS FILES ######### -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="{{ asset('js/lightslider/js/lightslider.min.js') }}"></script>
    <script src="{{ asset('js/featherlight/featherlight.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>

    <script>
    (function (window, $) {
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
    })(window, jQuery);
    </script>
    @section ('js_body')
    @show
</body>
</html>
