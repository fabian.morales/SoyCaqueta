@extends('master')

@section('js_header')
@stop

@section('content')                    

<div class="row">
    <div class="medium-6 columns">
        <h3>Recuperar clave</h3>
        <br />
        <p>Ingresa tu direcci&oacute;n de correo</p>
        <form method="post" action="{{ url('/cliente/clave/email') }}">
            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
            <div class="row">
                <div class="medium-3 columns"><label class="inline" for="email">Email</label></div>
                <div class="medium-9 columns"><input type="text" name="email" value="" /></div>
                <div class="small-12 columns"><input type="submit" value="Enviar" class="button default" /></div>
            </div>
        </form>
    </div>
</div>
@stop
