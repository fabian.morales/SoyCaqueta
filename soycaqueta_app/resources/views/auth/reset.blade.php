@extends('master')

@section('js_header')
@stop

@section('content')
<div class="row">
    <div class="small-6 columns">
        <h3>Cambia tu clave</h3>
        <div style="clear: both"></div>
        <br />
        <p>Diligencia este formulario para cambiar tu clave</p>
        <form method="post" action="{{ url('/cliente/clave/reset') }}">
            <input type="hidden" name="token" value="{{ $token }}" />
            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
            <div class="row">
                <div class="medium-3 columns"><label class="inline" for="email">Email</label></div>
                <div class="medium-9 columns"><input type="text" name="email" value="" class="form-control" /></div>
                <div class="medium-3 columns"><label class="inline" for="password_confirmation">Clave nueva</label></div>
                <div class="medium-9 columns"><input type="password" name="password" value="" class="form-control" /></div>
                <div class="medium-3 columns"><label class="inline" for="password_confirmation">Confirmar clave</label></div>
                <div class="medium-9 columns"><input type="password" name="password_confirmation" value="" class="form-control" /></div>
                <div class="medium-12 columns"><input type="submit" value="Cambiar clave" class="button default" /></div>
            </div>
        </form>
    </div>
</div>
@stop