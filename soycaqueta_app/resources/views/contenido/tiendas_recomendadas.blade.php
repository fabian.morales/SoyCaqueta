<div class="row tiendas_recomendadas">
    @foreach($tiendas as $t)
    <div class="col-sm-6 col-md-4">
        <div class="imagen">
            <img class="fullWidth" src="{{ asset('imagenes/tiendas/'.$t->id.'.png') }}" />
        </div>
        <p>
            <a href="{{ \App\Detalle::addhttp($t->url) }}" class="button agregar" target="_blank">
                <img src="{{ asset('/imagenes/template/icono-carrito-blanco.png') }}" />
                Visitar tienda
            </a>
        </p>
    </div>
    @endforeach
</div>
