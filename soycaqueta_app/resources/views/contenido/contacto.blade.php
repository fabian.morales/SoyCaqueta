<script>
(function (window, $) {
    function adicionarMarcador(mapa, lat, lng, info, img){
        var latlng = new google.maps.LatLng(lat, lng); 
        var marker = new google.maps.Marker({
                            position: latlng,
                            map: mapa,                                
                            html: info,
                            icon: img,
                            animation: google.maps.Animation.DROP, 
                            draggable: true
        });

        var infoWindow = new google.maps.InfoWindow({ content: marker.html });
        //infoWindow.open(mapa, marker);

        google.maps.event.addListener(marker, "click", function () {
            infoWindow.setContent(this.html);
            infoWindow.open(mapa, this);
        });
    }
    
    $(document).ready(function() {        
        var latlng = new google.maps.LatLng(4.6509313, -74.0638623); 
        var options = { zoom: 17, center: latlng, mapTypeId: google.maps.MapTypeId.ROADMAP}; 
        var map = new google.maps.Map(document.getElementById("mapa_canvas"), options);
        adicionarMarcador(map, 4.6509313, -74.0638623, '', undefined);
    });
})(window, jQuery);
</script>
<style>
#mapa_canvas{
    width: 100%;
    height: 400px;
    margin: 0 auto;    
}
</style>
<div class="row">
    <div class="col-md-6">
        <h3 class="titulo seccion"><span>Contacto</span></h3>
        <p><img src="{{ asset('/imagenes/whatsapp_verde.png') }}" width="24" height="24" />&nbsp;Tel&eacute;fono o Whatsapp: 304-400-0742</p>
        <p>Dirección de env&iacute;os: 18422 Via Di Sorrento,
            Boca Raton, Florida, 33496</p>
        <ul class='redes'>
            <li><a class='red facebook' href='https://www.facebook.com/encarguelo' target="_blank"><i class="fi-social-facebook"></i></a></li>
            <li><a class='red twitter' href='https://twitter.com/encarguelocom' target="_blank"><i class="fi-social-twitter"></i></a></li>
            <li><a class='red mail' href='mailto:info@encarguelo.com' target="_blank"><i class="fi-mail"></i></a></li>
        </ul>
    </div>
    <div class="col-md-6">
        <h3 class="titulo seccion"><span>Escr&iacute;banos cualquier duda o comentario</span></h3>
        <form id="form_contacto" name="form_contacto" method="post" action="{{ url('contacto/enviar') }}">
            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
            <label for="nombre">Nombre</label>
            <input type="text" id="nombre" name="nombre" />
            <label for="nombre">Email</label>
            <input type="text" id="email" name="email" />
            <label for="mensaje">Mensaje</label>
            <textarea id="mensaje" name="mensaje"></textarea>
            <input type="submit" class="button rosa" />
        </form>
    </div>
</div>
<div class="row fullWidth">
    <div class="col-sm-12">
        <div id="mapa_canvas"></div>
    </div>
</div>