<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        @yield('content')
        <br />
        <table style="width: 100%; display: block; padding: 10px 0;">
            <tr>
                <td><img src="{{ asset('imagenes/logo.png') }}" style="float: left; margin-right: 5px;" /></td>
                <td style="padding-left: 10px;">Encarguelo.com / Servicio al cliente <br />
                    info@encarguelo.com / 304-400-0742 <br />
                    Calle 65 #10-43 <br />
                    <a href="http://www.encarguelo.com" target="_blank">www.encarguelo.com</a>  <br />
                    <a href="http://twitter.com/encarguelocom" target="_blank"><img style="width: 16px; height: 16px;" src="{{ asset('imagenes/twitter_email.png') }}"</a>
                    <a href="http://facebook.com/encarguelo" target="_blank"><img style="width: 16px; height: 16px;" src="{{ asset('imagenes/facebook_email.png') }}"</a>
                </td>
            </tr>
        </table>
    </body>
</html>