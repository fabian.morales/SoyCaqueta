@extends('master')

@section('css_header')
<link href="{{ asset('js/featherlight/featherlight.min.css') }}" rel="stylesheet" />
@stop

@section('js_header')
<script src="{{ asset('js/featherlight/featherlight.min.js') }}"></script>
<script>
(function (window, $) {
    
    var $buscarCliente = false;
    $(document).ready(function(){                
        $("#form_registro").submit(function(e) {
            e.preventDefault();           
                        
            if ($("#nombre").val() === ''){
                alert('Debes ingresar tu nombre');
                $("#nombre").focus();
                return;
            }
            
            if ($("#apellido").val() === ''){
                alert('Debes ingresar tu apellido');
                $("#apellido").focus();
                return;
            }
            
            if ($("#email").val() === ''){
                alert('Debe singresar tu dirección de correo');
                $("#email").focus();
                return;
            }
            
            if (!cotizador.isEmail($("#email").val())){
                alert('La dirección de correo no es válida');
                $("#email").focus();
                return;
            }
            
            $.ajax({
                url: $(this).attr("action"),
                method: 'post',
                dataType: 'json',
                data: $(this).serialize()
            })
            .done(function(json) {
                if (json.ok === 1){
                    alert(json.mensaje);
                    window.location.reload();
                }
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseJSON.error.message);
            }).
            always(function() {
                $("#loader").removeClass("loading");
            });
        });
    });
})(window, jQuery);
</script>
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="small-12 columns">
            <h3 class="titulo seccion"><span>Tus datos</span></h3>
        </div>
    </div>
    <div class="row">
        <div class="medium-6 columns">
            <form id="form_registro" name="form_registro" action="{{ url('cliente/registro') }}" method="post">
                <input type="hidden" id="id" name="id" value="{{ $usuario->id }}" />
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <div class="row">
                    <div class="medium-4 columns">
                        <label for="email">Correo electr&oacute;nico<sup>*</sup></label>
                    </div>
                    <div class="medium-8 columns">
                        <input type="text" name="email" id="email" value="{{ $usuario->email }}" required />
                    </div>
                </div>
                <div class="row">
                    <div class="medium-4 columns">
                        <label for="nombre">Nombre<sup>*</sup></label>
                    </div>
                    <div class="medium-8 columns">
                        <input type="text" name="nombre" id="nombre" value="{{ $usuario->nombre }}"required />
                    </div>
                </div>

                <div class="row">
                    <div class="medium-4 columns"><label for="apellido">Apellidos<sup>*</sup></label></div>
                    <div class="medium-8 columns"><input type="text" id="apellido" name="apellido" value="@if(sizeof($usuario->cliente)){{ $usuario->cliente->apellido }}@endif" required /></div>        
                </div>

                <input type="hidden" name="login" id="login" value="{{ $usuario->login }}" />

                <div class="row">
                    <div class="medium-4 columns">
                        <label for="password">Clave<sup>*</sup></label>
                    </div>
                    <div class="medium-8 columns">
                        <input type="password" name="password" id="password" value="" required />
                    </div>
                </div>

                <div class="row">
                    <div class="medium-4 columns"><label for="nombre">Tel&eacute;fono</label></div>
                    <div class="medium-8 columns"><input type="text" id="telefono" name="telefono" value="@if(sizeof($usuario->cliente)){{ $usuario->cliente->telefono }}@endif" /></div>        
                </div>

                <div class="row">
                    <div class="small-12 columns">
                        <input type="submit" value="Actualizar datos" class="button" />
                    </div>
                </div>
            </form>
        </div>
    </div> 
</div>
@stop
