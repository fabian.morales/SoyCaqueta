@extends('master')

@section('content')
<form id="form_login" name="form_login" action="{{ url('/login') }}" method="post">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="container">
        
        <div class="row row-centered">
            <div class="col-sm-6 col-md-5 col-centered">
                <fieldset class="fieldset">
                    <legend>Inicio de sesi&oacute;n</legend>
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Nombre de usuario</label>
                        </div>
                        <div class="col-sm-8">
                            <input class="input-group-field" type="text" name="login" id="login" placeholder="Nombre de usuario" />    
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Nombre de usuario</label>
                        </div>
                        <div class="col-sm-8">
                            <input class="input-group-field" type="password" name="password" id="password" placeholder="Clave" />
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-sm-12">
                            <a href="{{ url('usuario/clave/email') }}">&iquest;Olvid&oacute; su clave?</a>&nbsp;
                            <a href="{{ url('usuario/registro') }}">Reg&iacute;strate</a><br />
                            <input type="submit" value="Iniciar sesi&oacute;n" class="button float-right" />
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
</form>
<br />
@stop