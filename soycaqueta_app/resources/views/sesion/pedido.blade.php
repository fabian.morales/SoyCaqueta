<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h3 class="titulo seccion"><span>Datos del pedido</span></h3>
        </div>
    </div>

    <div class="row">
        <div class="medium-2"><strong>Pedido:</strong></div>
        <div class="medium-10">{{ $pedido->id }}</div>

        <div class="medium-2"><strong>Fecha:</strong></div>
        <div class="medium-10">{{ $pedido->fecha_creacion }}</div>

        <div class="medium-2"><strong>Valor total:</strong></div>
        <div class="medium-10">$ {{ Helper::number_format($pedido->valor) }}</div>

        <div class="medium-2"><strong>Direcci&oacute;n:</strong></div>
        <div class="medium-10">{{ $pedido->direccion }}</div>

        <div class="medium-2"><strong>Ciudad:</strong></div>
        <div class="medium-10">{{ $pedido->ciudad }}</div>

        <div class="medium-2"><strong>Estado:</strong></div>
        <div class="medium-10">{{ $estados[$pedido->estado] }}</div>

        @if(!empty($pedido->guia_col))
        <div class="medium-2"><strong>N&uacute;m gu&iacute;a env&iacute;o a Colombia:</strong></div>
        <div class="medium-10">{{ $pedido->guia_col }}</div>
        @endif

        @if($pedido->estado == "U" || $pedido->estado == "F")
        <div class="medium-2"><strong>N&uacute;m gu&iacute;a:</strong></div>
        <div class="medium-10">{{ $pedido->guia }}</div>
        @endif
    </div>

    <div class="row separador">
        <div class="col-sm-12">
            <h3 class="titulo seccion"><span>Detalles</span></h3>
        </div>
    </div>

    <div class="row item lista head">
        <div class="col-sm-4"><strong>Nombre</strong></div>
        <div class="col-sm-3"><strong>Url</strong></div>
        <div class="col-sm-5"><strong>Valor</strong></div>
    </div>
    @foreach($pedido->detalles as $d)
    <div class="row item lista">
        <div class="col-sm-4">{{ $d->nombre }}</div>
        <div class="col-sm-3">@if(!empty($d->link))<a href="{{ \App\Detalle::addhttp($d->link) }}" target="_blank"><i class="fi-web"></i> Ver</a>@else &nbsp; @endif</div>
        <div class="col-sm-5">$ {{ Helper::number_format($d->valor) }}</div>
    </div>
    @endforeach
    <div class="row separador"></div>

    <div class="row">
        <div class="medium-2"><strong>Observaciones:</strong></div>
        <div class="medium-10">{{ $pedido->observaciones }}</div>
    </div>

    @if ($pedido->estado == 'C' && sizeof($pedido->tokens))
    <div class="row separador"></div>
    <div class="row">
        <div class="col-sm-12 text-right">
            <a href='{{ url('cotizacion/pagar/'.$pedido->tokens[0]->token) }}' target='_blank' class='button alert'>Paga aqu&iacute;</a>
        </div>
    </div>
    @endif

    @if ($pedido->estado == 'U')
    <div class="row separador"></div>
    <div class="row">
        <div class="col-sm-12 text-right">
            <a href='{{ url('cotizacion/encuesta/'.$pedido->tokens[0]->token) }}' class='button default'>Realizar encuesta de satisfacci&oacute;n</a>
        </div>
    </div>
    @endif    
</div>
