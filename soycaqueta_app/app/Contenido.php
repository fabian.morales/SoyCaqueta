<?php

namespace App;

class Contenido extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'con_contenido';
    protected $fillable = [
        'llave', 'titulo', 'tipo', 'cuerpo', 'peso', 'inicio', 'id_categoria', 'activo', 'destacado'
    ];
    
    public function categoria(){
        return $this->belongsTo('\App\Categoria', 'id_categoria');
    }
    
    public function autor(){
        return $this->hasOne('\App\User', 'id', 'id_autor');
    }
    
    public function comentarios(){
        return $this->hasMany('\App\Comentario', 'id_contenido');
    }
    
    public function valoraciones(){
        return $this->hasMany('\App\Valoracion', 'id_contenido');
    }
    
    public function generarToken($idComentario=""){
        $ret = new \stdClass();
        $ret->idContenido = $this->id;
        $ret->idComentario = $idComentario;
        $json = json_encode($ret);
        
        return base64_encode($json);
    }
    
    public function generarTokensVal(){
        
        $tokens = [];
        
        for ($i=1;$i<=5;$i++){
            $ret = new \stdClass();
            $ret->idContenido = $this->id;
            $ret->valor = $i;
            $json = json_encode($ret);
            $tokens[] = base64_encode($json);
        }
        
        return $tokens;
    }
}
