<?php

namespace App;

class Categoria extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'con_categoria';
    protected $fillable = [
        'id', 'llave', 'nombre', 'tipo', 'layout'
    ];
    
    public function contenidos(){
        return $this->hasMany("\App\Contenido", "id_categoria");
    }
}
