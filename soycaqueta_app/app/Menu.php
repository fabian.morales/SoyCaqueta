<?php

namespace App;

class Menu extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'con_menu';
    
    protected $fillable = [
        'titulo', 'peso', 'id_seccion', 'url', 'llave'
    ];
    
    public function seccion(){
        return $this->hasOne("\App\Contenido", "id", "id_seccion");
    }
}