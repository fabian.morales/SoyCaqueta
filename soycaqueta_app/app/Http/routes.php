<?php

/*
  |--------------------------------------------------------------------------
  | Routes File
  |--------------------------------------------------------------------------
  |
  | Here is where you will register all of the routes in an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */
//Route::pattern('id', '[0-9]+');

Route::get('/crearAdmin', function() {
    $usuario = App\User::where("email", "fabian.morales@outlook.com")->first();
    //$usuario->nombre = "Admin";
    //$usuario->login = "admin";
    $usuario->password = \Hash::make("Ev4nerv");
    $usuario->admin = "Y";
    print_r($usuario);
    $usuario->save();
});

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | This route group applies the "web" middleware group to every route
  | it contains. The "web" middleware group is defined in your HTTP
  | kernel and includes session state, CSRF protection, and more.
  |
 */

//Route::group(['middleware' => ['web']], function () {
    
    Route::get('/', 'SeccionController@mostrarIndex');
    
    Route::get('/login', 'SesionController@mostrarIndex');
    Route::post('/login', 'SesionController@hacerLogin');
    Route::get('/logout', 'SesionController@hacerLogout');
       
    Route::any('/seccion/{key}', 'SeccionController@mostrarSeccionMenu');
    Route::any('/contenido/{key}', 'SeccionController@mostrarSeccion');
    Route::any('/contacto/enviar', 'SeccionController@enviarContacto');
    
    Route::get('/categoria/{categoria}', 'SeccionController@mostrarCategoria');
    Route::get('/comentar/{token}', 'SeccionController@mostrarFormComentario');
    Route::get('/valorar/{token}', 'SeccionController@guardarValoracion');
    Route::post('/comentar', 'SeccionController@guardarComentario');
    
    Route::get('/editorial', 'SeccionController@mostrarEditorial');
    Route::get('/blog', 'SeccionController@mostrarBlog');
    Route::get('/blog/{categoria}', 'SeccionController@mostrarBlogCat');
       
    Route::group(['prefix' => 'usuario', 'as' => 'cliente::'], function() {
        Route::get('/registro', 'SesionController@crearUsuario');
        Route::post('/registro', 'SesionController@registrarUsuario');
        Route::get('/perfil', 'SesionController@mostrarPerfil')->middleware(['auth']);
               
        Route::get('/clave/email', 'Auth\PasswordController@getEmail');
        Route::post('/clave/email', 'Auth\PasswordController@postEmail');
        
        Route::get('/clave/reset/{token}', 'Auth\PasswordController@getReset');
        Route::post('/clave/reset', 'Auth\PasswordController@postReset');
    });
    
    Route::group(['prefix' => 'noticias', 'as' => 'noticia::'], function() {
        Route::get('/', 'SeccionController@mostrarNoticiasCat');
        Route::get('/{categoria}/{noticia}', 'SeccionController@mostrarNoticia');
        Route::get('/general/{noticia}', 'SeccionController@mostrarNoticia');
    });
    
    Route::group(array('prefix' => 'administrador', 'as' => 'admin::', 'middleware' => ['permisos']), function() {
        Route::get('/', 'Admin\AdminController@mostrarIndex');
                
        Route::group(array('prefix' => 'categoria', 'as' => 'categoria::'), function() {
            Route::get('/', ['as' => 'lista', 'uses' => 'Admin\CategoriaController@mostrarIndex']);
            Route::get('/crear', ['as' => 'crear', 'uses' => 'Admin\CategoriaController@crearCategoria']);
            Route::get('/editar/{id}', ['as' => 'editar', 'uses' => 'Admin\CategoriaController@editarCategoria']);
            Route::post('/guardar', ['as' => 'guardar', 'uses' => 'Admin\CategoriaController@guardarCategoria']);
        });
        
        Route::group(array('prefix' => 'contenido', 'as' => 'contenido::'), function() {
            Route::get('/', ['as' => 'listaEmail', 'uses' => 'Admin\ContenidoController@mostrarIndex']);
            Route::get('/editar/{key}', ['as' => 'editarEmail', 'uses' => 'Admin\ContenidoController@mostrarFormContenidoEmail']);
            Route::post('/guardar', ['as' => 'guardar', 'uses' => 'Admin\ContenidoController@guardarContenido']);
            Route::get('/estado/{id}/{estado}', ['as' => 'estado', 'uses' => 'Admin\ContenidoController@establecerEstado']);
            
            Route::group(array('prefix' => 'seccion', 'as' => 'seccion::'), function() {
                Route::get('/', ['as' => 'lista', 'uses' => 'Admin\ContenidoController@mostrarListaSecciones']);
                Route::get('/crear', ['as' => 'crear', 'uses' => 'Admin\ContenidoController@crearSeccion']);
                Route::get('/editar/{id}', ['as' => 'editar', 'uses' => 'Admin\ContenidoController@editarSeccion']);
            });
            
            Route::group(array('prefix' => 'noticia', 'as' => 'noticia::'), function() {
                Route::get('/', ['as' => 'lista', 'uses' => 'Admin\ContenidoController@mostrarListaNoticias']);
                Route::get('/crear', ['as' => 'crear', 'uses' => 'Admin\ContenidoController@crearNoticia']);
                Route::get('/editar/{id}', ['as' => 'editar', 'uses' => 'Admin\ContenidoController@editarNoticia']);
            });
        });
        
        Route::group(array('prefix' => 'menus'), function() {
            Route::get('/', 'Admin\MenuController@mostrarIndex');
            Route::get('/crear', 'Admin\MenuController@crearMenu');
            Route::get('/editar/{id}', 'Admin\MenuController@editarMenu');
            Route::post('/guardar', 'Admin\MenuController@guardarMenu');
            Route::get('/borrar/{id}', 'Admin\MenuController@borrarMenu');
        });
        
        Route::group(array('prefix' => 'testimonios'), function() {
            Route::get('/', 'Admin\TestimonioController@mostrarIndex');
            Route::get('/crear', 'Admin\TestimonioController@crearTest');
            Route::get('/editar/{id}', 'Admin\TestimonioController@editarTest');
            Route::post('/guardar', 'Admin\TestimonioController@guardarTest');
        });

        Route::group(array('prefix' => 'faqs'), function() {
            Route::get('/', 'Admin\FaqController@mostrarIndex');
            Route::get('/crear', 'Admin\FaqController@crearFaq');
            Route::get('/editar/{id}', 'Admin\FaqController@editarFaq');
            Route::post('/guardar', 'Admin\FaqController@guardarFaq');
        });

        Route::group(array('prefix' => 'usuarios'), function() {
            Route::get('/', 'Admin\UsuarioController@mostrarIndex');
            Route::get('/crear', 'Admin\UsuarioController@crearUsuario');
            Route::get('/editar/{id}', 'Admin\UsuarioController@editarUsuario');
            Route::post('/guardar', 'Admin\UsuarioController@guardarUsuario');
        });
    });
    
    if (!Request::is("forum")){
        Route::any('/{key}', 'SeccionController@mostrarSeccionMenu');
    }
//});
