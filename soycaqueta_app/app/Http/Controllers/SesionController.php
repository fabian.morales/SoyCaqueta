<?php

namespace App\Http\Controllers;

use \Illuminate\Support\Facades\Input;
use \Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;

class SesionController extends Controller {

    public function mostrarIndex(){
        if (\Auth::check()){
            return $this->mostrarPerfil();
        }
        return \View::make('sesion.login');
    }
       
    public function hacerLogin(){
        $login = Input::get("login");
        $clave = Input::get("password");
        if (\Auth::attempt(array('login' => $login, 'password' => $clave))){
            if (\Auth::user()->admin == 'Y'){
                return \Redirect::action("Admin\AdminController@mostrarIndex")->with("mensaje", "Logueado");
            }
            else{
                return \Redirect::back()->with("mensaje", "Logueado");
            }
        }
        else{
            return \Redirect::action("SesionController@mostrarIndex")->with("mensajeError", "Usuario o clave incorrecta");
        }
    }
    
    public function hacerLogout(){
        \Auth::logout();
        return \Redirect::action("SesionController@mostrarIndex");
    }
    
    public function mostrarFormUsuario($usuario) {
        if (!sizeof($usuario)) {
            $usuario = new \App\User();
        }

        return \View::make("sesion.form_registro", array("usuario" => $usuario));
    }

    public function crearUsuario() {
        return $this->mostrarFormUsuario(new \App\User());
    }

    public function editarUsuario($id) {
        $usuario = \App\User::find($id);
        if (!sizeof($usuario)) {
            return \Redirect::action('SesionController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el usuario");
        }

        return $this->mostrarFormUsuario($usuario);
    }
    
    public function registrarUsuario(){
        try{
            $id = Input::get("id");
            $clave = Input::get("password");
            $nuevo = false;

            $usuario = \App\User::find($id);
            if (!sizeof($usuario)) {
                $usuario = new \App\User();
                $nuevo = true;
            }

            $usuario->fill(Input::all());

            if (empty($usuario->nombre)) {
                throw new \Exception('Debe ingresar su nombre');
            }

            if (empty($id) && empty($clave)) {
                throw new \Exception('Debe ingresar su clave');
            } 
            else if (!empty($clave)) {
                $clave = \Illuminate\Support\Facades\Hash::make($clave);
            } 
            else {
                $clave = $usuario->password;
            }

            $usuario->password = $clave;

            $cntEmail = \App\User::where("email", $usuario->email)->where("id", "<>", (int)$usuario->id)->count();
            if ($cntEmail > 0) {
                throw new \Exception('Ya existe un usuario con el correo ingresado');
            }

            $cntLogin = \App\User::where("login", $usuario->login)->where("id", "<>", (int)$usuario->id)->count();
            if ($cntLogin > 0) {
                throw new \Exception('Ya existe un usuario con el nombre de usuario ingresado');
            }

            if ($usuario->save()) {
                if ($nuevo){
                    $datosEmail = [
                        "usuario" => $usuario,
                    ];

                    Mail::send('emails.usuarios.registro_cliente', $datosEmail, function($message) use ($usuario) {
                        $message->from('info@encarguelo.com', 'Encarguelo.com');
                        $message->subject('Tu registro ha sido exitoso');
                        $message->to($usuario->email, $usuario->nombre);
                        $message->bcc('desarrollo@encubo.ws');
                    });
                    
                    return json_encode(["ok" => 1, "mensaje" => $nuevo ? "Tu registro ha sido exitoso" : "Datos actualizados correctamente"]);
                }
                else{
                    throw new \Exception('No se pudo realizar el registro');
                }

            }
            else {
                throw new \Exception('No se pudo realizar el registro');
            }
        }
        catch (\Exception $ex) {
            return $this->retornarError($ex);
        }
    }
    
    public function mostrarPerfil(){
        return \View::make('sesion.perfil', ["usuario" => \Auth::user()]);
    }
}