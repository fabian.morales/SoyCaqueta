<?php

namespace App\Http\Controllers;

use \Illuminate\Support\Facades\Input;
use \Illuminate\Support\Facades\Mail;
use \Illuminate\Support\Facades\DB;

class SeccionController extends Controller {
    use \App\SeccionTrait;
    
    public function mostrarIndex() {
        
        $destacadas = \App\Contenido::with(["categoria", "autor"])->where("tipo", "N")->where("destacado", "S")->where("activo", "S")->orderBy("created_at", "desc")->take(10)->get();
        $recientes = \App\Contenido::with(["categoria", "autor"])
            ->whereHas('categoria', function($q) {
                $q->where('layout','N');
            })
            ->where("tipo", "N")
            ->where("activo", "S")
            ->orderBy("created_at", "desc")
            ->take(3)
            ->get();
                
        $populares = \App\Contenido::with(["categoria", "autor"])
            ->whereHas('categoria', function($q) {
                $q->where('layout','N');
            })
            ->where("tipo", "N")
            ->where("activo", "S")
            ->orderBy(DB::raw("suma_puntaje/conteo_puntaje"), "desc")
            ->orderBy("created_at", "desc")
            ->take(3)
            ->get();
                
        $visitadas = \App\Contenido::with(["categoria", "autor"])
            ->whereHas('categoria', function($q) {
                $q->where('layout','N');
            })
            ->where("tipo", "N")->where("activo", "S")
            ->orderBy("visitas", "desc")
            ->orderBy("created_at", "desc")
            ->take(4)
            ->get();
            
        $secciones = $this->obtenerSeccionesInicio();
        $menus = \App\Menu::where("id", "<=", 3)->get();
        $vars = [            
            "destacadas" => $destacadas,
            "recientes" => $recientes,
            "populares" => $populares,
            "visitadas" => $visitadas,
            "secciones" => $secciones, 
            "menus" => $menus
        ];
        return \View::make('seccion.index', $vars);
    }
    
    public function mostrarSeccion($key){
        if (empty($key) || $key == '{% producto.imagen %}'){
            return \Redirect::action('SeccionController@mostrarIndex');
        }
        
        $seccionObj = \App\Contenido::where("llave", $key)->first();
        
        if (!sizeof($seccionObj)){
            return \Redirect::action('SeccionController@mostrarIndex')->with("mensajeError", "Sección no encontrada (0)");
        }
                
        $seccion = $this->obtenerSeccion($seccionObj);
        
        $template =  \Illuminate\Support\Facades\Request::ajax() ? "seccion.contenido_raw" : "seccion.contenido";
        
        return \View::make($template, ["seccion" => $seccion, "menu" => null]);
    }
    
    public function mostrarSeccionMenu($key){
        if (empty($key) || $key == '{% producto.imagen %}'){
            return \Redirect::action('SeccionController@mostrarIndex');
        }
        
        $menu = \App\Menu::with("seccion")->where("llave", $key)->first();
        
        if (!sizeof($menu)){
            return \Redirect::action('SeccionController@mostrarIndex')->with("mensajeError", "Sección no encontrada (1) - ".$key);
        }
        
        if (!sizeof($menu->seccion)){
            return \Redirect::action('SeccionController@mostrarIndex')->with("mensajeError", "Sección no encontrada (2) - ".$key);
        }
        
        $seccion = $this->obtenerSeccion($menu->seccion);
        
        $template =  \Illuminate\Support\Facades\Request::ajax() ? "seccion.contenido_raw" : "seccion.contenido";
        
        return \View::make($template, ["seccion" => $seccion, "menu" => $menu]);
    }
    
   public function enviarContacto(){
        try{
            $nombre = Input::get("nombre");
            $email = Input::get("email");
            $mensaje = Input::get("mensaje");
            
            if(empty($nombre)){
                return \Redirect::action('SeccionController@mostrarIndex')->with("mensajeError", "Debe ingresar el nombre");
            }
            
            if(empty($email)){
                return \Redirect::action('SeccionController@mostrarIndex')->with("mensajeError", "Debe ingresar el correo");
            }
            
            if(empty($mensaje)){
                return \Redirect::action('SeccionController@mostrarIndex')->with("mensajeError", "Debe ingresar el mensaje");
            }
            
            $datosEmail = [
                'nombre' => $nombre,
                'email' => $email,
                'mensaje' => $mensaje,
            ];
                        
            Mail::send('emails.contacto.contacto', $datosEmail, function($message) {
                $message->from('info@encarguelo.com', 'Encarguelo.com');
                $message->subject('Solicitud de contacto');
                $message->to('info@encarguelo.com', 'Encarguelo.com');
                $message->bcc('desarrollo@encubo.ws');
            });
            
            return \Redirect::action('SeccionController@mostrarIndex')->with("mensaje", "Hemos recibido tu solicitud de contacto. Pronto nos pondremos en contacto contigo.");
                    
        }
        catch (\Exception $ex) {
            return $this->retornarError($ex);
        }
    }
    
    public function mostrarNoticiasCat(){
        $noticias = \App\Contenido::with('categoria')
            ->where(function($q) {
                $q->whereHas('categoria', function($q) {
                    $q->where('layout','N');
                })
                ->orWhereNull('id_categoria');
            })
            ->where('tipo', 'N')
            ->orderBy('created_at', 'desc')
            ->take(100)
            ->paginate(18);
            
        return \View::make("seccion.layout_noticias", ['noticias' => $noticias]);
    }
    
    public function mostrarBlog(){
        $noticias = \App\Contenido::with('categoria')
            ->whereHas('categoria', function($q) {
                $q->where('layout','B');
            })
            ->where('tipo', 'N')
            ->orderBy('created_at', 'desc')
            ->take(100)
            ->paginate(18);
        
        $categorias = \App\Categoria::where('layout' ,'B')->orderBy('nombre')->get();
        
        $fechas = DB::table('contenido')
            ->whereHas('categoria', function($q) {
                $q->where('layout','B');
            })
            ->where('tipo', 'N')
            ->select(DB::raw('YEAR(date) year'), DB::raw('MONTH(date) month'))
            ->groupBy(DB::raw('YEAR(date) year'))
            ->groupBy(DB::raw('MONTH(date) month'));
            
        return \View::make("seccion.layout_blog", ['noticias' => $noticias, 'categorias' => $categorias, 'fechas' => $fechas]);
    }
    
    public function mostrarBlogCat($categoria){
        
        $objCategoria = [];
        
        if (!empty($categoria) && $categoria != 'general'){
            list($idCat, $llaveCat) = explode('-', $categoria, 2);
            $objCategoria = \App\Categoria::where("id", $idCat)->where("llave", $llaveCat)->first();
            if (!sizeof($objCategoria)){
                return \Redirect::action('SeccionController@mostrarIndex')->with("mensajeError", "Categoría no válida");
            }
        }
        
        $noticias = \App\Contenido::with('categoria')
            ->whereHas('categoria', function($q) use ($objCategoria) {
                $q->where('id', $objCategoria->id);
            })
            ->where('tipo', 'N')
            ->orderBy('created_at', 'desc')
            ->take(100)
            ->paginate(18);
        
        $categorias = \App\Categoria::where('layout' ,'B')->orderBy('nombre')->get();
        
        $fechas = DB::table('contenido')
            ->whereHas('categoria', function($q) {
                $q->where('layout','B');
            })
            ->where('tipo', 'N')
            ->select(DB::raw('YEAR(date) year'), DB::raw('MONTH(date) month'))
            ->groupBy(DB::raw('YEAR(date) year'))
            ->groupBy(DB::raw('MONTH(date) month'));
            
        return \View::make("seccion.layout_blog", ['noticias' => $noticias, 'categorias' => $categorias, 'fechas' => $fechas]);
    }
    
    public function mostrarEditorial(){
        $noticias = \App\Contenido::with('categoria')
            ->whereHas('categoria', function($q) {
                $q->where('layout','E');
            })
            ->where('tipo', 'N')
            ->orderBy('created_at', 'desc')
            ->take(100)
            ->paginate(18);
            
        return \View::make("seccion.layout_editorial", ['noticias' => $noticias]);
    }
    
    public function mostrarNoticia($categoria="", $noticia=""){
        
        $objCategoria = [];
        
        if (!empty($categoria) && $categoria != 'general'){
            list($idCat, $llaveCat) = explode('-', $categoria, 2);
            $objCategoria = \App\Categoria::where("id", $idCat)->where("llave", $llaveCat)->first();
            if (!sizeof($objCategoria)){
                return \Redirect::action('SeccionController@mostrarIndex')->with("mensajeError", "Categoría no válida");
            }
        }
        
        list($idNot, $llaveNot) = explode('-', $noticia, 2);
        
        $objNoticia = \App\Contenido::with(["categoria", "autor", "comentarios" => function($q) {
            $q->where("estado", "A")->whereNull("id_comentario")->orderBy("created_at", "desc");
        },
        "comentarios.usuario", 
        "valoraciones" => function($q) {
            if (\Auth::check()){
                $q->where("id_usuario", \Auth::user()->id)->take(1);
            }
            else{
                $q->where("id_usuario", -1)->take(1);
            }
            
        }])->where("id", $idNot)->where("llave", $llaveNot)->where("tipo", "N")->first();
        if (!sizeof($objNoticia)){
            return \Redirect::action('SeccionController@mostrarIndex')->with("mensajeError", "Noticia no v�lida");
        }
        
        if (sizeof($objCategoria) && $objNoticia->id_categoria != $objCategoria->id){
            return \Redirect::action('SeccionController@mostrarIndex')->with("mensajeError", "Categoría no v�lida");
        }
        
        $objNoticia->visitas += 1;
        $objNoticia->save();
        
        $recientes = \App\Contenido::with(["categoria", "autor"])
            ->whereHas('categoria', function($q) {
                $q->where('layout','N');
            })
            ->where("tipo", "N")
            ->where("activo", "S")
            ->orderBy("created_at", "desc")
            ->take(3)
            ->get();
        $visitadas = \App\Contenido::with(["categoria", "autor"])
            ->whereHas('categoria', function($q) {
                $q->where('layout','N');
            })
            ->where("tipo", "N")
            ->where("activo", "S")
            ->orderBy("visitas", "desc")
            ->orderBy("created_at", "desc")
            ->take(4)
            ->get();
        
        $vars = [            
            "noticia" => $objNoticia,
            "recientes" => $recientes,
            "categoria" => $objCategoria,
            "visitadas" => $visitadas
        ];
        
        return \View::make("seccion.noticia", $vars);
    }
    
    public function mostrarFormComentario($token){
        if (!\Auth::check()){
            return \Redirect::action('SesionController@mostrarIndex')->with("mensajeError", "Debe iniciar sesión para poder comentar");
        }
        
        return \View::make("seccion.form_comentario", [ "token" => $token ]);
    }
    
    public function guardarComentario(){
        if (!\Auth::check()){
            return \Redirect::action('SesionController@mostrarIndex')->with("mensajeError", "Debe iniciar sesión para poder comentar");
        }
        
        $token = Input::get("token");
        $json = base64_decode($token);
        $info = json_decode($json);
        
        $idContenido = $info->idContenido;
        $idComentario = !empty($info->idComentario) ? $info->idComentario : null;        
        $contenido = Input::get("comentario");
        
        $comentario = new \App\Comentario();
        $comentario->id_usuario = \Auth::user()->id;
        $comentario->id_contenido = $idContenido;
        $comentario->id_comentario = $idComentario;
        $comentario->contenido = $contenido;
        $comentario->estado = 'A';
        
        if ($comentario->save()){
            $noticia = \App\Contenido::find($idContenido);
            $noticia->num_comentarios += 1;
            $noticia->save();
            
            return \Redirect::back()->with("mensaje", "Gracias por tu comentario");
        }
        else{
            return \Redirect::back()->with("mensajeError", "No se pudo guardar el comentario, intente nuevamente");
        }
    }
    
    public function guardarValoracion($token){
        if (!\Auth::check()){
            return \Redirect::action('SesionController@mostrarIndex')->with("mensajeError", "Debe iniciar sesión para poder valorar una noticia");
        }
        
        $json = base64_decode($token);
        $info = json_decode($json);
        
        $idContenido = $info->idContenido;
        $valor = $info->valor;
        
        $val = new \App\Valoracion();
        $val->id_usuario = \Auth::user()->id;
        $val->id_contenido = $idContenido;
        $val->valor = $valor;
        
        if ($val->save()){
            $noticia = \App\Contenido::find($idContenido);
            $noticia->conteo_puntaje += 1;
            $noticia->suma_puntaje += $valor;
            $noticia->promedio_puntaje = round($noticia->suma_puntaje / $noticia->conteo_puntaje, 2);
            $noticia->save();
            
            return \Redirect::back()->with("mensaje", "Gracias por tu aporte");
        }
        else{
            return \Redirect::back()->with("mensajeError", "No se pudo guardar la valoración, intente nuevamente");
        }
    }
}