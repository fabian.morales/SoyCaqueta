<?php

namespace App\Http\Controllers\Admin;

use \Illuminate\Support\Facades\Input;

class ContenidoController extends AdminController {
    
    var $destino1 = "desarrollo@encubo.ws"; //"info@encarguelo.com";
    var $destino2 = "desarrollo@encubo.ws";

    public function mostrarIndex() {
        $emails = [
            "pedido_nuevo_cli" => "Pedido nuevo - Cliente",
            "pedido_nuevo_cli_car" => "Pedido nuevo carrito - Cliente",
            "pedido_nuevo_adm" => "Pedido nuevo - Admin",
            "pedido_cotizado" => "Pedido cotizado",
            "pedido_pagado" => "Pedido pagado (solo pagos en línea)",
            "pedido_comprado" => "Pedido comprado",
            "pedido_enviado_alm" => "Pedido enviado por almacen",
            "pedido_recibido_eeuu" => "Pedido recibido en bodega EEUU",
            "pedido_enviado_col" => "Pedido enviado a Colombia",
            "pedido_recibido_col" => "Pedido recibido en Colombia",
            "encuesta_enviada" => "Encuesta de satisfacción enviada",
            "respuesta_encuesta" => "Respuesta de encuesta recibida"
        ];
        return \View::make("admin.contenido.index", ["emails" => $emails]);
    }
    
    public function mostrarFormContenidoEmail($key){
        $contenido = \App\Contenido::where("llave", $key)->first();
        return \View::make('admin.contenido.form_email', ["contenido" => $contenido]);
    }
    
    public function guardarContenido(){
        $id = Input::get("id");
        $contenido = \App\Contenido::find($id);
        
        if (!sizeof($contenido)){
            $contenido = new \App\Contenido();
        }
        
        $contenido->fill(Input::all());
        
        if ($contenido->tipo != "E"){
            $car = ["á", "é", "í", "ó", "ú", "ï", "ü", " ", ",", "/", "?", "¿", "!", "¡"];
            $rep = ["a", "e", "i", "o", "u", "i", "u", "-", "-", "", "", "", "", ""];
        }
        
        $contenido->llave = str_replace($car, $rep, strtolower($contenido->titulo));
        
        if (empty($contenido->inicio)){
            $contenido->inicio = 'N';
        }
        
        if (empty($contenido->destacado)){
            $contenido->destacado = 'N';
        }
        
        if (empty($contenido->id_categoria)){
            $contenido->id_categoria = null;
        }
        
        $acciones = [
            "E" => "Admin\ContenidoController@mostrarIndex",
            "S" => "Admin\ContenidoController@mostrarListaSecciones",
            "N" => "Admin\ContenidoController@mostrarListaNoticias",
            "B" => "Admin\ContenidoController@mostrarListaBlog",
        ];
        
        $action = $acciones[$contenido->tipo];
        
        if($contenido->save()){
            if (Input::hasFile('imagen') && Input::file('imagen')->isValid()) {
                Input::file('imagen')->move(public_path('imagenes/contenido'), $contenido->id.'.jpg');
            }
            
            return \Redirect::action($action)->with("mensaje", "Contenido guardado exitosamente");
        }
        else{
            return \Redirect::action($action)->with("mensajeError", "No se pudo guardar el contenido");
        }
    }
    
    public function establecerEstado($id, $estado){
        $contenido = \App\Contenido::find($id);
        
        if (!sizeof($contenido)){
            return \Redirect::action('Admin\ContenidoController@mostrarListaSecciones')->with("mensajeError", "No se pudo encontrar el contenido");
        }
        
        $acciones = [
            "E" => "Admin\ContenidoController@mostrarIndex",
            "S" => "Admin\ContenidoController@mostrarListaSecciones",
            "N" => "Admin\ContenidoController@mostrarListaNoticias",
            "B" => "Admin\ContenidoController@mostrarListaBlog",
        ];
        
        $contenido->activo = $estado;
        $mensaje = ($estado == "S" ? "publicado" : "despublicado");
        $action = $acciones[$contenido->tipo];
        
        if($contenido->save()){
            return \Redirect::action($action)->with("mensaje", "Contenido ".$mensaje." exitosamente");
        }
        else{
            return \Redirect::action($action)->with("mensajeError", "No se pudo establecer el contenido como".$mensaje);
        }
    }
    
    public function mostrarListaSecciones(){
        $secciones = \App\Contenido::where("tipo", "S")->get();
        return \View::make('admin.contenido.lista_secciones', ["secciones" => $secciones]);
    }
    
    public function crearSeccion(){
        return $this->mostrarFormContenidoSeccion(new \App\Contenido());
    }
    
    public function editarSeccion($id){
        $contenido = \App\Contenido::find($id);
        
        if (!sizeof($contenido)){
            return \Redirect::action('Admin\ContenidoController@mostrarListaSecciones')->with("mensajeError", "No se pudo encontrar la sección");
        }
        
        return $this->mostrarFormContenidoSeccion($contenido);
    }

    public function mostrarFormContenidoSeccion($contenido){
        return \View::make('admin.contenido.form_seccion', ["contenido" => $contenido]);
    }
    
    public function mostrarListaNoticias(){
        $noticias = \App\Contenido::with("categoria")->where("tipo", "N")->orderBy("created_at", "desc")->get();
        $categorias = \App\Categoria::where("tipo", "N")->get();
        return \View::make('admin.contenido.lista_noticias', ["noticias" => $noticias, "categorias" => $categorias]);
    }
    
    public function crearNoticia(){
        return $this->mostrarFormNoticia(new \App\Contenido());
    }
    
    public function editarNoticia($id){
        $noticia = \App\Contenido::find($id);
        
        if (!sizeof($noticia)){
            return \Redirect::action('Admin\ContenidoController@mostrarListaNoticias')->with("mensajeError", "No se pudo encontrar la noticia");
        }
        
        return $this->mostrarFormNoticia($noticia);
    }

    public function mostrarFormNoticia($noticia){
        $categorias = \App\Categoria::all();
        return \View::make('admin.contenido.form_noticia', ["noticia" => $noticia, "categorias" => $categorias]);
    }
}
