<?php

namespace App\Http\Controllers\Admin;

use \Illuminate\Support\Facades\Input;

class CategoriaController extends AdminController {
    
    public function mostrarIndex() {
        $tipos = [
            "N" => "Noticias",
            "B" => "Blog",
            "E" => "Editorial"
        ];
        
        $categorias = \App\Categoria::paginate(20);
        
        return \View::make("admin.categoria.index", ["categorias" => $categorias, "tipos" => $tipos]);
    }
    
    public function guardarCategoria(){
        $id = Input::get("id");
        $categoria = \App\Categoria::find($id);
        
        if (!sizeof($categoria)){
            $categoria = new \App\Categoria();
        }
        
        $categoria->fill(Input::all());
        
        if ($categoria->tipo != "E"){
            $car = ["á", "é", "í", "ó", "ú", "ï", "ü", " ", ",", "/", "?", "¿", "!", "¡"];
            $rep = ["a", "e", "i", "o", "u", "i", "u", "-", "-", "", "", "", "", ""];

            $categoria->llave = str_replace($car, $rep, strtolower($categoria->nombre));
        }
        
        if($categoria->save()){
            return \Redirect::action("Admin\CategoriaController@mostrarIndex")->with("mensaje", "Categoría guardada exitosamente");
        }
        else{
            return \Redirect::action("Admin\CategoriaController@mostrarIndex")->with("mensajeError", "No se pudo guardar el contenido");
        }
    }
    
    public function crearCategoria(){
        return $this->mostrarFormCategoria(new \App\Categoria());
    }
    
    public function editarCategoria($id){
        $categoria = \App\Categoria::find($id);
        
        if (!sizeof($categoria)){
            return \Redirect::action('Admin\CategoriaController@mostrarIndexs')->with("mensajeError", "No se pudo encontrar la categoría");
        }
        
        return $this->mostrarFormCategoria($categoria);
    }

    public function mostrarFormCategoria($categoria){
        $tipos = [
            "N" => "Noticias",
            "B" => "Blog",
            "E" => "Editorial"
        ];
        
        return \View::make('admin.categoria.form', ["categoria" => $categoria, "tipos" => $tipos]);
    }
}
