<?php

namespace App;

class Valoracion extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'con_valoracion';
    
    public function usuario(){
        return $this->belongsTo("\App\User", "id_usuario");
    }
    
    public function contenido(){
        return $this->belongsTo("\App\Contenido", "id_contenido");
    }
}
