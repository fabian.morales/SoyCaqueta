<?php

namespace App;

class Faq extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'con_faq';
    protected $fillable = [
        'titulo', 'descripcion', 'peso', 'activo'
    ];
}
