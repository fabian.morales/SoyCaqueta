<?php

namespace App;

trait SeccionTrait{
        
    public function obtenerSeccionesInicio(){
        $secciones = Contenido::where("tipo", "S")->where("inicio", "S")->orderBy("peso")->get();
        $ret = [];
        foreach ($secciones as $s){
            $ret[] = $this->obtenerSeccion($s);
        }
        
        return $ret;
    }
    
    public function obtenerSeccion($seccion, $idSeccion=null){
        $url = url('/cotizacion/enviar');
        $formCotizacion = "";
        $formContacto = "";
        $sliderTestimonios = "";
        $gridTiendas = "";
        $acordeonFaqs = "";
        $bloqueLinks = "";
        
        if (!sizeof($seccion)){
            $seccion = Contenido::find($idSeccion);
        }
        
        if (strpos($seccion->cuerpo, "{!! \$form_cotizacion !!}") !== false){
            $formCotizacion = \View::make('cotizador.form_light', ["url_cotizacion" => $url])->render();
        }
        
        if (strpos($seccion->cuerpo, "{!! \$form_contacto !!}") !== false){
            $formContacto = \View::make('contenido.contacto')->render();
        }
        
        if (strpos($seccion->cuerpo, "{!! \$slider_testimonios !!}") !== false){
            $testimonios = Testimonio::where('activo', 'S')->get();
            $sliderTestimonios = \View::make('contenido.testimonios', ["testimonios" => $testimonios])->render();
        }
        
        if (strpos($seccion->cuerpo, "{!! \$grid_tiendas !!}") !== false){
            $tiendas = Tienda::where('activo', 'S')->orderBy("titulo")->get();
            $gridTiendas = \View::make('contenido.tiendas_recomendadas', ["tiendas" => $tiendas])->render();
        }
        
        if (strpos($seccion->cuerpo, "{!! \$acordeon_faqs !!}") !== false){
            $faqs = Faq::where('activo', 'S')->orderBy("peso")->orderBy("titulo")->get();
            $acordeonFaqs = \View::make('contenido.faqs', ["faqs" => $faqs])->render();
        }
        
        if (strpos($seccion->cuerpo, "{!! \$bloque_links !!}") !== false){
            $menus = Menu::where("id", "<=", 3)->get();
            $bloqueLinks = \View::make('seccion.bloque_links', ["menus" => $menus])->render();
        }
        
        $vars = [
            "form_cotizacion" => $formCotizacion,
            "form_contacto" => $formContacto,
            "slider_testimonios" => $sliderTestimonios,
            "grid_tiendas" => $gridTiendas,
            "acordeon_faqs" => $acordeonFaqs,
            "bloque_links" => $bloqueLinks
        ];
        
        $html = \Blade::compileString($seccion->cuerpo);
        
        return $this->renderSeccion($html, $vars);
    }
    
    function renderSeccion($__php, $__data)
    {
        $obLevel = ob_get_level();
        ob_start();
        extract($__data, EXTR_SKIP);
        try {
            eval('?' . '>' . $__php);
        } catch (Exception $e) {
            while (ob_get_level() > $obLevel) ob_end_clean();
            throw $e;
        } catch (Throwable $e) {
            while (ob_get_level() > $obLevel) ob_end_clean();
            throw new FatalThrowableError($e);
        }
        return ob_get_clean();
    }
}