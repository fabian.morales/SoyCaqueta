<?php

namespace App;

use \Illuminate\Support\Facades\Mail;

trait MailTrait{
    
    var $destino1 = "info@encarguelo.com";
    //var $destino1 = "soporte.myc@gmail.com";
    var $destino2 = "soporte.myc@gmail.com";
    
    public function enviarMensajePedidoId($idPedido, $key, $emailDestino, $nombreDestino){
        $pedido = \App\Pedido::where('id', $idPedido)->with(['cliente', 'detalles', 'tokens'])->first();
        $this->enviarMensajePedido($pedido, $key, $emailDestino, $nombreDestino);
    }
    
    public function enviarMensajePedido($pedido, $key, $emailDestino, $nombreDestino){
        if (empty($key)){
            $llaves = [
                'N' => 'pedido_nuevo_cli',
                'C' => 'pedido_cotizado',
                'P' => '',
                'O' => 'pedido_comprado',
                'A' => 'pedido_enviado_alm',
                'R' => 'pedido_recibido_eeuu',
                'E' => 'pedido_enviado_col',
                'I' => 'pedido_recibido_col',
                'U' => 'encuesta_enviada',
                'F' => 'respuesta_enviada'
            ];
            
            $key = $llaves[$pedido->estado];
            
            if ($pedido->estado == 'P'){
                return;
            }
            
            if ($pedido->estado == 'F'){
                $emailDestino = $this->destino1;
                $nombreDestino = 'Encarguelo.com';
            }
            else{
                $emailDestino = $pedido->cliente->email;
                $nombreDestino = $pedido->cliente->nombre;
            }
        }
        
        $email = $this->obtenerMensaje($pedido, $key);
        
        Mail::send('emails.general', ["contenido" => $email["cuerpo"]], function($message) use($email, $emailDestino, $nombreDestino) {
            $message->from($this->destino1, 'Encarguelo.com');
            $message->subject($email["titulo"]);
            $message->to($emailDestino, $nombreDestino);
            $message->bcc($this->destino2);
        });
    }
    
    public function obtenerMensaje($pedido, $keyCorreo){
        $tablaDetalles = \View::make('emails.detalle_pedido', ["pedido" => $pedido])->render();
        
        $linkPago = "";
        if ($pedido->estado == "C" && sizeof($pedido->tokens)){
            $linkPago = \View::make('emails.link_pago', ["token" => $pedido->tokens[0]])->render();
        }
        
        $linkEncuesta = "";
        if (($pedido->estado == "I" || $pedido->estado == "U") && sizeof($pedido->tokens)){
            $linkEncuesta = \View::make('emails.link_encuesta', ["pedido" => $pedido])->render();
        }
        
        $vars = [
            "tabla_detalles" => $tablaDetalles,
            "numero_pedido" => $pedido->id,
            "fecha_pedido" => $pedido->fecha_creacion,
            "estado_pedido" => Pedido::estados()[$pedido->estado],
            "valor_pedido" => Helpers\Helper::number_format($pedido->valor),
            "nombre_cliente" => $pedido->nombre_cliente,
            "apellido_cliente" => $pedido->apellido_cliente,
            "correo_cliente" => $pedido->cliente->email,
            "telefono_cliente" => $pedido->telefono_cliente,
            "observaciones" => $pedido->observaciones,
            "direccion_entrega" => $pedido->direccion,
            "ciudad_entrega" => $pedido->ciudad,
            "guia" => $pedido->guia,
            "guia_col" => $pedido->guia_col,
            "link_pago" => $linkPago,
            "link_encuesta" => $linkEncuesta
        ];
        
        $contenido = Contenido::where("llave", $keyCorreo)->where("tipo", "E")->first();
        $html = \Blade::compileString($contenido->cuerpo);
        
        return ["titulo" => $contenido->titulo, "cuerpo" => $this->render($html, $vars)];
    }
    
    function render($__php, $__data)
    {
        $obLevel = ob_get_level();
        ob_start();
        extract($__data, EXTR_SKIP);
        try {
            eval('?' . '>' . $__php);
        } catch (Exception $e) {
            while (ob_get_level() > $obLevel) ob_end_clean();
            throw $e;
        } catch (Throwable $e) {
            while (ob_get_level() > $obLevel) ob_end_clean();
            throw new FatalThrowableError($e);
        }
        return ob_get_clean();
    }
}