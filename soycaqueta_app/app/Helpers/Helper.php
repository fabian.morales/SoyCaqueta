<?php

namespace App\Helpers;

class Helper
{
    public static function number_format($value)
    {
        return number_format((float)$value, 0, ",", ".");
    }
    
    public static function obtenerShipping($url){
        $parser = new \App\Lib\myParser();
        
        
        if (substr($url, 0, 5) != 'https'){
            $url = str_replace('http', 'https', $url);
        }
        
        //$html = $parser->parsear(urldecode($url));
        $header = [];
	$header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,"; 
	$header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5"; 
	$header[] = "Cache-Control: max-age=0"; 
	$header[] = "Connection: keep-alive"; 
	$header[] = "Keep-Alive: 300"; 
	$header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7"; 
	$header[] = "Accept-Language: en-us,en;q=0.5"; 
	$header[] = "Pragma: "; //browsers keep this blank. 
        
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.3) Gecko/20100401 Firefox/3.6.3'); 
	curl_setopt($ch, CURLOPT_HTTPHEADER, $header); 
	curl_setopt($ch, CURLOPT_REFERER, 'http://www.google.com'); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        $content = curl_exec($ch);
        curl_close($ch);
        
        
        
        echo "<textarea>$content/textarea>";
        die();
        $dom = $html->find('span#ourprice_shippingmessage');
        $encontrado = false;
        
        if (sizeof($dom)){
            $encontrado = true;
        }
        
        if (!$encontrado){
            $dom = $html->find('span#saleprice_shippingmessage');
            
            if (sizeof($dom)){
                $encontrado = true;
            }
        }
        
        if (!$encontrado){
            $dom = $html->find('#usedbuyBox .a-row');
            
            if (sizeof($dom)){
                echo "222222";
                $encontrado = true;
            }
        }
        
        /*if (!$encontrado){
            $dom = $html->find('div#buyNewInner span.buyboxShippingLabel a');
            
            if (sizeof($dom)){
                $encontrado = true;
            }
        }*/
        
        if (!$encontrado){
            return 0;
        }
        
        $span = strtolower($dom[0]->innerHtml);
        
        echo $span;
        echo $html;
        if (strpos($span, 'free shipping') !== false){
            return 0;
        }
        else{
            $matches = [];        
            preg_match('!\d+\.*\d*!', $span, $matches);
            return $matches[0];
        } 
    }
    
    public static function calcularPrecioVenta($precio, $peso){
        if ($precio < 299){
            $res = round(($precio + ($precio * 0.16) + ($peso * 3.4)) * 100) / 100;
        }
        else{
            $res = round(($precio + ($precio * 0.13) + ($peso * 3.4)) * 100) / 100;
        }
        
        return $res;
    }
    
    public static function obtenerListaestados(){
        $ret = [
            'N' => 'Nuevo',
            'C' => 'Cotizado',
            'P' => 'Pagado',
            'O' => 'Comprado',
            'A' => 'Enviado a almacen',
            'R' => 'Recibido bodega EEUU',
            'E' => 'Enviado a Colombia',
            'I' => 'Recibido bodega Colombia',
            'U' => 'Encuesta enviada',
            'F' => 'Finalizado'
        ];
        
        return $ret;
    }
}