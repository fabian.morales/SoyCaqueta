<?php

namespace App;

class Comentario extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'con_comentario';
    
    public function usuario(){
        return $this->belongsTo("\App\User", "id_usuario");
    }
    
    public function contenido(){
        return $this->belongsTo("\App\Contenido", "id_contenido");
    }
    
    public function padre(){
        return $this->belongsTo("\App\Comentario", "id_comentario");
    }
    
    public function hijos(){
        return $this->hasMany("\App\Comentario", "id_comentario");
    }
}
