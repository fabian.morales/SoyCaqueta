(function (window, $, numeral, $v) {
    
    var $vCarrito = null;

    var cotizador = {
        /*cargarForm: function($target) {
            $.ajax({
                url: '/cotizador/light',
                method: 'get',
                success: function(res){
                    $($target).html(res);
                    hookForm();
                }
            });
        },*/
        
        isEmail: function(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        },
        
        mostrarModalCarrito: function($producto){
            $vCarrito.producto = $producto;
            //alert($vCarrito.producto.nombre);
        }
    };
    
    window.cotizador = cotizador;
    
    function scrollTo(target) {
        var ooo = $(target).offset().top;
        console.log($(target));
        $('html, body').animate({scrollTop: ooo}, 600);
    }
    
    $(document).ready(function() {
        $v.config.delimiters = ['{%', '%}'];
        $vCarrito = new $v({
            el: '#producto-agregado',
            data: {
                producto : null
            },
            watch: {
                'producto': function (val, oldVal) {
                    $.featherlight($('#producto-agregado'));
                }
            }
        });
        
        $(window).resize(function() {
            $(".menu-resp > a").each(function(i, o) {
                var $target = $(o).attr("href");
                if (!$(o).is(":visible")){
                    $($target).show();
                }
                else{
                    $($target).hide();
                }
            });
        });
        
        $("#btnAdicionarReg").click(function (e) {
            e.preventDefault();
            var $html = $("#detalle").html();
            var $o = $($.parseHTML($html));
                        
            var $cnt = $("#detalles > div.row").size() + 1;
            var $txtCnt = $o.find("input[rel='cantidad_detalle']");
            var $btnMas = $o.find("button[rel='btnMas']");
            var $btnMenos = $o.find("button[rel='btnMenos']");
            
            $txtCnt.attr("id", "cantidad_detalle_" + $cnt);
            $btnMas.attr("data-target", "cantidad_detalle_" + $cnt);
            $btnMenos.attr("data-target", "cantidad_detalle_" + $cnt);
            
            $btnMas.click(function(e) {
                e.preventDefault();
                var $target = "#" + $(this).attr("data-target");
                var $valor = parseInt($($target).val()) + 1;
                $($target).val($valor);
            });
            
            $btnMenos.click(function(e) {
                e.preventDefault();
                var $target = "#" + $(this).attr("data-target");
                var $valor = $($target).val();
                if ($valor > 1){
                    $($target).val($valor - 1);
                }
            });
            
            /*hookRegistro($o, true);*/
            $("#detalles").append($o);
        });
        
        $("button[rel='btnMas']").click(function(e) {
            e.preventDefault();
            var $target = "#" + $(this).attr("data-target");
            var $valor = parseInt($($target).val()) + 1;
            $($target).val($valor);
        });

        $("button[rel='btnMenos']").click(function(e) {
            e.preventDefault();
            var $target = "#" + $(this).attr("data-target");
            var $valor = $($target).val();
            if ($valor > 1){
                $($target).val($valor - 1);
            }
        });
        
        $("#btnEnviarCotizacion").click(function(e) {
            e.preventDefault();
            $("#btnEnviarCotizacion").prop("disabled", true);
                        
            if ($("#nombre").val() === ''){
                alert('Debe ingresar su nombre');
                $("#nombre").focus();
                $("#btnEnviarCotizacion").prop("disabled", false);
                return;
            }
            
            if ($("#apellido").val() === ''){
                alert('Debe ingresar su apellido');
                $("#apellido").focus();
                $("#btnEnviarCotizacion").prop("disabled", false);
                return;
            }
            
            if ($("#email").val() === ''){
                alert('Debe ingresar su dirección de correo');
                $("#email").focus();
                $("#btnEnviarCotizacion").prop("disabled", false);
                return;
            }
            
            if ($("#ciudad").val() === ''){
                alert('Debe ingresar la ciudad de destino');
                $("#ciudad").focus();
                $("#btnEnviarCotizacion").prop("disabled", false);
                return;
            }
            
            if (!cotizador.isEmail($("#email").val())){
                alert('La dirección de correo no es válida');
                $("#email").focus();
                $("#btnEnviarCotizacion").prop("disabled", false);
                return;
            }
            
            var $error = false;
            $("input[rel=nombre_detalle]").each(function(i, o) {
                if ($(o).val() === ''){
                    alert('Debe ingresar todos los nombre de los productos');
                    $("#btnEnviarCotizacion").prop("disabled", false);
                    $error = true;
                    return;
                }
            });
            
            if ($error === true){
                return;
            }
            
            $("input[rel=url_detalle]").each(function(i, o) {
                if ($(o).val() === '' && $("#observaciones").val() === ''){
                    alert('Hay productos sin enlace y tampoco se ha especificado datos en las observaciones');
                    $error = true;
                    $("#btnEnviarCotizacion").prop("disabled", false);
                    return;
                }
            });
            
            if ($error === true){
                return;
            }
            
            $("#loader").addClass("loading");
            $.ajax({
                url: $("#form_cotizacion").attr("action"),
                method: 'post',
                data: $("#form_cotizacion").serialize()
            })
            .done(function(res) {
                $("#loader").removeClass("loading");
                alert('Hemos recibido tu cotización y te la estaremos enviando en máximo 12 horas hábiles. No olvides revisar tu carpeta de no deseados.');
                window.location.href = '/gracias';
                //window.location.reload();
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                $("#loader").removeClass("loading");
                $("#btnEnviarCotizacion").prop("disabled", false);
                alert(jqXHR.responseJSON.error.message);
            });
        });
        
        $("#telefono").keypress(function(e) {
            if (e.which < 48 || e.which > 57){
                e.preventDefault();
            }
        });
        
        $("#btnConfirmarPedido").click(function(e) {
            e.preventDefault();
                        
            if ($("#email").val() === ''){
                alert('Debe ingresar su dirección de correo');
                $("#email").focus();
                return;
            }
            
            $("#loader").addClass("loading");
            $.ajax({
                url: $("#form_carrito").attr("action"),
                method: 'post',
                data: $("#form_carrito").serialize()
            })
            .done(function(res) {
                $("#loader").removeClass("loading");
                window.location.href = res;
                //window.location.reload();
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                $("#loader").removeClass("loading");
                alert(jqXHR.responseJSON.error.message);
            });
        });
        
        $("#btnCalcular").click(function(e) {
            e.preventDefault();
            var $precio = parseFloat($("#precio_calcula").val());
            var $peso = parseFloat($("#peso_calcula").val());
            
            if ($precio <= 0){
                alert('Debe ingresar el precio');
                $("#precio_calcula").focus();
                return;
            }

            if ($peso <= 0){
                alert('Debe ingresar el peso');
                $("#peso_calcula").focus();
                return;
            }
            
            $("#loader").addClass("loading");
            
            $.ajax({
                url: '/buscador/tasa',
                method: 'get',
                dataType: 'json'
            })
            .done(function(json) {
                var $tasa = parseFloat(json.tasa);
                var $res = 0;
                if ($precio < 299){
                    $res = Math.round(($precio + ($precio * 0.16) + ($peso * 3.4)) * $tasa * 100) / 100;
                }
                else{
                    $res = Math.round(($precio + ($precio * 0.13) + ($peso * 3.4)) * $tasa * 100) / 100;
                }

                $("#res_calcula").val(numeral($res).format('$0,0').replace(',', '.')); 
                $("#loader").removeClass("loading");
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                $("#loader").removeClass("loading");
                alert(jqXHR.responseJSON.error.message);
            });
        });
        
        if ($("ul.testimonios").size() > 0){
            $("ul.testimonios").lightSlider({
                item: 1,
                loop: true,
                adaptiveHeight: true
            });
        }
        
        if ($("ul#slider_destacados").size() > 0){
            var $slider = $("ul#slider_destacados").lightSlider({
                //autoWidth:true,
                //adaptiveHeight: true,
                item: 4,
                pager: false,
                loop: false,
                controls: true,
                responsive : [
                    {
                        breakpoint:1030,
                        settings: {
                            item:3,
                            slideMove:1
                          }
                    },
                    {
                        breakpoint:800,
                        settings: {
                            item:2,
                            slideMove:1
                          }
                    },
                    {
                        breakpoint:320,
                        settings: {
                            item:1,
                            slideMove:1
                          }
                    }
                ]
            });
            
            $("#lnkDestIzq").click(function(e){
                e.preventDefault();
                $slider.goToPrevSlide();                
            });
            
            $("#lnkDestDer").click(function(e){
                e.preventDefault();
                $slider.goToNextSlide();
            });
        }
        
        $("a[rel='scroll']").click(function(e){
            e.preventDefault();
            var $target = $(this).attr("href");            
            scrollTo($target);
        });
        
        if ($("a[rel='lnkActualizarItem']").size() > 0){
            $("a[rel='lnkActualizarItem']").click(function(e) {
                e.preventDefault();
                $("#loader").addClass("loading");
                var $idCantidad = $(this).attr("data-reg");
                var $idProducto = $(this).attr("data-producto");
                var $cantidad = parseInt($($idCantidad).val());
                $.ajax({
                    url: $(this).attr("href"),
                    method: 'post', 
                    data: {
                        id_producto: $idProducto,
                        cantidad: $cantidad,
                        no_suma: 1
                    }
                })
                .done(function(json) {
                    window.location.reload();
                    $("#loader").removeClass("loading");
                })
                .fail(function (jqXHR, textStatus, errorThrown) {
                    $("#loader").removeClass("loading");
                    alert(jqXHR.responseJSON.error.message);
                });
            });
        }
        
        if ($("#form_contacto").size() > 0){
            $("#form_contacto").submit(function(e) {
                if ($("#nombre").val() === ''){
                    alert('Debe ingresar su nombre');
                    $("#nombre").focus();
                    e.preventDefault();
                    return;
                }

                if ($("#email").val() === ''){
                    alert('Debe ingresar su dirección de correo');
                    $("#email").focus();
                    e.preventDefault();
                    return;
                }
                
                if ($("#mensaje").val() === ''){
                    alert('Debe ingresar su el mensaje');
                    $("#email").focus();
                    e.preventDefault();
                    return;
                }

                if (!cotizador.isEmail($("#email").val())){
                    alert('La dirección de correo no es válida');
                    $("#email").focus();
                    e.preventDefault();
                    return;
                }
            });
        }
        
        $(".menu-resp > a").click(function(e) {
            e.preventDefault();
            var $target = $(this).attr("href");
            $($target).toggle("slow");
            console.log($($target));
        });
        
        
    });
})(window, jQuery, numeral, Vue);
